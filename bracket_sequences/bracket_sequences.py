def check_sequence(sequence: str) -> bool:
    bracket_stack = list()
    pairs = {"}": "{", ")": "(", "]": "["}

    for s in sequence:
        if s == "{" or s == "(" or s == "[":
            bracket_stack.append(s)
        elif s == "}" or s == ")" or s == "]":
            if len(bracket_stack) == 0:
                return False
            if pairs[s] != bracket_stack.pop():
                return False

    if len(bracket_stack) != 0:
        return False
    return True
