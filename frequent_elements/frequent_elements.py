from typing import List, DefaultDict, Tuple
from collections import defaultdict
from queue import PriorityQueue

# See https://leetcode.com/problems/top-k-frequent-elements
def most_frequent_numbers(nums: List[int], k: int) -> List[int]:
    element_counter: DefaultDict[int, int] = defaultdict(int)
    for x in nums:
        element_counter[x] += 1

    q: PriorityQueue[Tuple[int, int]] = PriorityQueue()
    for key, val in element_counter.items():
        q.put((-val, key))

    res = []
    for _ in range(k):
        res.append(q.get()[1])
    return res
