from heapq import heappush, heappop

# See https://leetcode.com/problems/find-median-from-data-stream/
class MedianFinder:
    def __init__(self):
        self.right_heap = []
        self.left_heap = []

    def add_num(self, num: int):
        heappush(self.left_heap, -num)
        heappush(self.right_heap, -heappop(self.left_heap))
        if len(self.right_heap) > len(self.left_heap):
            heappush(self.left_heap, -heappop(self.right_heap))

    def find_median(self) -> float:
        if len(self.right_heap) == 0 and len(self.left_heap) == 0:
            return 0
        if len(self.right_heap) != len(self.left_heap):
            return -self.left_heap[0]
        else:
            return (self.right_heap[0] - self.left_heap[0]) / 2
