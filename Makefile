.PHONY: test
test:
	python3 -m unittest discover -v ./
	nosetests ./ --with-coverage

.PHONY: lint
lint:
	python3 -m black --check ./
	python3 -m mypy ./
