import unittest

from frequent_elements.frequent_elements import most_frequent_numbers


class TestCheckSequence(unittest.TestCase):
    def test_most_frequent_numbers(self):
        self.assertEqual(most_frequent_numbers(nums=[1, 1, 1, 2, 2, 3], k=2), [1, 2])
        self.assertEqual(most_frequent_numbers(nums=[1], k=1), [1])
