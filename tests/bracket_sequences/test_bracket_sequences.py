import unittest

from bracket_sequences.bracket_sequences import check_sequence


class TestCheckSequence(unittest.TestCase):
    def test_positive_scenario(self):
        self.assertEqual(check_sequence("(hello)"), True)
        self.assertEqual(check_sequence("(qw{qw[qw]qw}qw)"), True)

    def test_negative_scenario(self):
        self.assertEqual(check_sequence("(22+3)*(21/[34+1)]"), False)
        self.assertEqual(check_sequence("(()"), False)
        self.assertEqual(check_sequence("}{"), False)
        self.assertEqual(check_sequence("(]"), False)
