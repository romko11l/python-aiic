import unittest

from median.median import MedianFinder


class TestMedianFinder(unittest.TestCase):
    def test_finder(self):
        median_finder = MedianFinder()
        self.assertEqual(median_finder.find_median(), 0)
        median_finder.add_num(1)
        median_finder.add_num(2)
        self.assertEqual(median_finder.find_median(), 1.5)
        median_finder.add_num(3)
        self.assertEqual(median_finder.find_median(), 2)
