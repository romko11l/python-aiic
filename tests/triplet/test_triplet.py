import unittest

from triplet.triplet import bruteforce_triplet_search


class TestCheckSequence(unittest.TestCase):
    def test_bruteforce_triplet_search(self):
        self.assertEqual(
            bruteforce_triplet_search(arr=[3, 0, 1, 1, 9, 7], a=7, b=2, c=3), 4
        )
        self.assertEqual(
            bruteforce_triplet_search(arr=[1, 1, 2, 2, 3], a=0, b=0, c=1), 0
        )
        self.assertEqual(
            bruteforce_triplet_search(arr=[1, 1, 1, 1, 1], a=0, b=0, c=0), 10
        )
        self.assertEqual(bruteforce_triplet_search(arr=[1, 1], a=0, b=0, c=0), 0)
        self.assertEqual(bruteforce_triplet_search(arr=[1, 1, 1], a=0, b=0, c=0), 1)
