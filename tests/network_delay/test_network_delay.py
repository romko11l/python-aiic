import unittest

from network_delay.network_delay import network_delay_time


class TestNetworkDelayTime(unittest.TestCase):
    def test_network_delay_time(self):
        self.assertEqual(network_delay_time([[2, 1, 1], [2, 3, 1], [3, 4, 1]], 4, 2), 2)
        self.assertEqual(network_delay_time([[1, 2, 1]], 2, 1), 1)
        self.assertEqual(network_delay_time([[1, 2, 1]], 2, 2), -1)
