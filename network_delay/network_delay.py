from typing import List, Tuple, Dict
import heapq
import collections


def build_graph(times: List[List[int]]) -> Dict[int, List[Tuple[int, int]]]:
    res = collections.defaultdict(list)
    for start, end, path_len in times:
        res[start].append((end, path_len))
    return res


def dijkstra(graph: Dict[int, List[Tuple[int, int]]], k: int) -> Dict[int, int]:
    res = dict()
    pq = [(0, k)]
    while len(pq) != 0:
        step, start = heapq.heappop(pq)
        if start in res:
            continue
        res[start] = step
        for end, distance in graph[start]:
            if end not in res:
                heapq.heappush(pq, (step + distance, end))
    return res


# See https://leetcode.com/problems/network-delay-time/
def network_delay_time(times: List[List[int]], n: int, k: int) -> int:
    graph = build_graph(times)
    if k not in graph:
        return -1

    path_len_dict = dijkstra(graph, k)
    if len(path_len_dict) != n:
        return -1
    return max(path_len_dict.values())
