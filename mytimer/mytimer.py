from datetime import datetime
import time


class MyTimer:
    def __init__(self, units="m"):
        self.units = units

    def __enter__(self) -> MyTimer:
        self.start_time = datetime.now()
        return self

    def __exit__(self, exc_type, exc_value, exc_traceback):
        self.end_time = datetime.now()
        self.spent_time = self.end_time - self.start_time

    def elapsed_time(self):
        if self.units == "s":
            return self.spent_time.total_seconds()
        if self.units == "m":
            return self.spent_time.total_seconds() / 60.0
        if self.units == "h":
            return self.spent_time.total_seconds() / 3600.0


with MyTimer(units="s") as t:
    time.sleep(1)
    print("Hello world")

print(t.elapsed_time())
